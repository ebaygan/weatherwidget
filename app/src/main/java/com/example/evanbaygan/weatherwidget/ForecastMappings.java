package com.example.evanbaygan.weatherwidget;

import java.util.HashMap;

/**
 * Created by evanbaygan on 10/22/14.
 */
public class ForecastMappings {
    private HashMap<String, String> climaconMap = new HashMap<String, String>();
    private HashMap<String, Integer> colorMap = new HashMap<String, Integer>();
    private static String CLEAR_DAY = "clear-day";
    private static String CLEAR_NIGHT = "clear-night";
    private static String RAIN = "rain";
    private static String SNOW = "snow";
    private static String SLEET = "sleet";
    private static String WIND = "wind";
    private static String FOG = "fog";
    private static String CLOUDY = "cloudy";
    private static String PARTLY_CLOUDY_DAY = "partly-cloudy-day";
    private static String PARTLY_CLOUDY_NIGHT = "partly-cloudy-night";

    public ForecastMappings() {
        climaconMap.put(CLEAR_DAY, "I");
        climaconMap.put(CLEAR_NIGHT, "S");
        climaconMap.put(RAIN, "$");
        climaconMap.put(SNOW, "6");
        climaconMap.put(SLEET, "9");
        climaconMap.put(WIND, "D");
        climaconMap.put(FOG, "?");
        climaconMap.put(CLOUDY, "!");
        climaconMap.put(PARTLY_CLOUDY_DAY, "\"");
        climaconMap.put(PARTLY_CLOUDY_NIGHT, "#");

        colorMap.put(CLEAR_DAY, R.color.clearday_blue);
        colorMap.put(CLEAR_NIGHT, R.color.clearnight_purple);
        colorMap.put(RAIN, R.color.rainy_blue);
        colorMap.put(SNOW, R.color.snow_blue);
        colorMap.put(SLEET, R.color.sleet_white);
        colorMap.put(WIND, R.color.yellow_wind);
        colorMap.put(FOG, R.color.fog_gray);
        colorMap.put(CLOUDY, R.color.cloudy_turquoise);
        colorMap.put(PARTLY_CLOUDY_DAY, R.color.partly_cloudy_day_turquoise);
        colorMap.put(PARTLY_CLOUDY_NIGHT, R.color.partly_cloudy_night_blue);
    }

    public String getForecastMapping(String key) {
        String returnResult = climaconMap.get(key);
        return (returnResult != null) ? returnResult : "I";
        //if the mapping is not found return clear day as default
    }

    public int getBorderColor(String key) {
        Integer borderColor = colorMap.get(key);
        return (borderColor != null) ? borderColor : R.color.clearday_blue;
    }
}
