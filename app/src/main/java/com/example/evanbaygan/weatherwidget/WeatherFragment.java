package com.example.evanbaygan.weatherwidget;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.forecast.io.network.responses.INetworkResponse;
import com.forecast.io.network.responses.NetworkResponse;
import com.forecast.io.toolbox.NetworkServiceTask;
import com.forecast.io.v2.network.services.ForecastService;
import com.forecast.io.v2.transfer.LatLng;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by evanbaygan on 10/21/14.
 */
public class WeatherFragment extends Fragment implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {
    @InjectView(R.id.climacon_text) TextView climaconView;
    @InjectView(R.id.circular_background) de.hdodenhof.circleimageview.CircleImageView background;
    @InjectView(R.id.clock) TextClock clock;
    @InjectView(R.id.status) TextView statusText;
    private static TextView dayText;
    private static TextView dateText;
    private LocationClient locationClient;
    LocationRequest locationRequest;
    private static int UPDATE_INTERVAL_MS = 5000;
    private static int FASTEST_UPDATE_INTERVAL_MS = 1000;
    static final String FORECAST_IO_APIKEY = "57956013fce8683b789969e57bd8d162";
    Typeface climaconFont;
    ForecastMappings mappings = new ForecastMappings();
    private Integer currentColor;

    public WeatherFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_weather, container, false);
        ButterKnife.inject(this, rootView);
        dateText = ButterKnife.findById(rootView, R.id.date);
        dayText = ButterKnife.findById(rootView, R.id.day_text);
        updateDateTextViews();
        initializeDateUpdate();
        initializeLocationClient();
        climaconFont = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Climacons.ttf");
        climaconView.setTypeface(climaconFont);
        final AtomicBoolean isBusy = new AtomicBoolean(false);
        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isBusy.get()) {
                    showBusyStatus();
                    isBusy.set(true);
                } else {
                    if (currentColor != null) {
                        background.setBorderColor(getResources().getColor(currentColor));
                        background.clearAnimation();
                        statusText.setText("");
                        isBusy.set(false);
                    }
                }
            }
        });
        return rootView;
    }

    private void initializeLocationClient() {
        locationClient = new LocationClient(getActivity(), this, this);
        locationClient.connect();
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL_MS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_MS);
    }

    public static void updateDateTextViews() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        switch (day){
            case 0:
                dateText.setText("MONDAY");
                break;
            case 1:
                dateText.setText("TUESDAY");
                break;
            case 2:
                dateText.setText("WEDNESDAY");
                break;
            case 3:
                dateText.setText("THURSDAY");
                break;
            case 4:
                dateText.setText("FRIDAY");
                break;
            case 5:
                dateText.setText("SATURDAY");
                break;
            case 6:
                dateText.setText("SUNDAY");
                break;
            default:
                dateText.setText("Invalid DAY");
                break;
        }
        String formattedDate = new SimpleDateFormat( "MM/dd/yyyy" ).format(cal.getTime());
        dayText.setText(formattedDate);
    }

    private void initializeDateUpdate() {
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getActivity(), UpdateDateBroadcastReceiver.class);
        alarmManager.set(AlarmManager.RTC_WAKEUP, AlarmManager.INTERVAL_DAY, PendingIntent.getBroadcast(getActivity(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT));
    }

    private void getWeeklyForecast(Location location) {
        LatLng.Builder latLangBuilder = LatLng.newBuilder();
        latLangBuilder.setLatitude(location.getLatitude());
        latLangBuilder.setLongitude(location.getLongitude());


        ForecastService.Request request = ForecastService.Request.newBuilder(FORECAST_IO_APIKEY)
                .setLatLng(latLangBuilder.build())
                .build();

        new NetworkServiceTask() {

            @Override
            protected void onPostExecute( INetworkResponse network ) {
                if ( network == null || network.getStatus() == NetworkResponse.Status.FAIL ) {
                    Toast.makeText( getActivity(), "FORECAST ERROR", Toast.LENGTH_SHORT ).show();

                    return;
                }

                ForecastService.Response response = ( ForecastService.Response ) network;
                String icon = response.getForecast().getCurrently().getIcon();

                Toast.makeText( getActivity(), response.getForecast() != null ?
                       icon : "FORECAST", Toast.LENGTH_SHORT ).show();
                climaconView.setText(mappings.getForecastMapping(icon));
                currentColor = mappings.getBorderColor(icon);
                background.setBorderColor(getResources().getColor(currentColor));
            }
        }.execute(request);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = locationClient.getLastLocation();
        getWeeklyForecast(location);
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static class UpdateDateBroadcastReceiver extends BroadcastReceiver {
        public UpdateDateBroadcastReceiver() {}
        public void onReceive(Context context, Intent intent) {
            updateDateTextViews();
        }
    }

    private void showBusyStatus() {
        background.setBorderColor(getResources().getColor(R.color.red));
        background.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.pulse_anim));
        statusText.setText(getString(R.string.busy_text));
        statusText.setTextColor(getResources().getColor(R.color.red));
        statusText.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in_and_out));
    }
}